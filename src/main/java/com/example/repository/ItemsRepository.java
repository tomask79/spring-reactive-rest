package com.example.repository;

/**
 * Created by Tomas.Kloucek on 17.3.2017.
 */
import com.example.domain.Item;
import org.springframework.data.jpa.repository.JpaRepository;

// all spring data jpa repos are transactional, will be scanned by spring, see Config.
public interface ItemsRepository extends JpaRepository<Item, String> {

    /**
     * Spring data-jpa code generated method for finding
     * @param id
     * @return
     */
    Item findById(final String id);
}
