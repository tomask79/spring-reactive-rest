package com.example.controller;

import com.example.domain.Item;
import com.example.service.IItemsService;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

/**
 * Created by Tomas.Kloucek on 17.3.2017.
 */
@RestController
public class ItemsReactiveController {

    @Autowired
    private IItemsService iItemsService;

    public Flux<Item> findById(String id) {
        try {
            System.out.println("Getting the data from DB...");
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Flux.just(iItemsService.findById(id));
        //return Flux.just(iItemsService.findById(id)).publishOn(Schedulers.parallel());
    }

    @GetMapping(value = "/store/{id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public void getItemById(@PathVariable String id) {
        System.out.println("Controller start....");
        findById(id).subscribe(new Subscriber<Item>() {

            @Override
            public void onSubscribe(Subscription s) {
                s.request(Long.MAX_VALUE);
            }
            @Override
            public void onNext(Item t) {
                System.out.println("Retrieved: "+t.getName());
            }
            @Override
            public void onError(Throwable t) {
            }
            @Override
            public void onComplete() {
            }
        });
        /**
        findById(id).subscribe(v -> {
           System.out.println("Consumed: "+v.getId());
        });
         */
        System.out.println("End of method.");
    }
}
