package com.example.service;

import com.example.domain.Item;
import com.example.repository.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Tomas.Kloucek on 17.3.2017.
 */
@Service
public class ItemsService implements IItemsService {
    @Autowired
    private ItemsRepository itemsRepository;

    public Item findById(String id) {
        return itemsRepository.findById(id);
    }
}
