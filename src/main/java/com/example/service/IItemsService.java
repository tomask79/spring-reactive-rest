package com.example.service;

import com.example.domain.Item;

/**
 * Created by Tomas.Kloucek on 17.3.2017.
 */
public interface IItemsService {
    public Item findById(String id);
}
