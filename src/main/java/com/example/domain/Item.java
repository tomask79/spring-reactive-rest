package com.example.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Tomas.Kloucek on 17.3.2017.
 */
@Entity
@Table(name="ITEMS")
public class Item {
    @Id
    protected String id;

    @Column(name="ITEM_NAME")
    protected String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}