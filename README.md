# Spring WebFlux and Reactive programming #

After seeing Jurgen Hoeller introducing [new Spring 5 features](https://www.jfokus.se/jfokus17/preso/Spring-Framework-5--Themes-and-Trends.pdf) I finally pushed myself into trying new Spring WebFlux project provided now in the not yet released Spring Boot 2.0.0 Snapshot. Let's start:

## Maven WebFlux project generation ##
* Go to [Spring boot app generator](https://start.spring.io)
* In the Spring Boot version select "2.0.0 (Snapshot)"
* In the dependencies search for "*Reactive Web*"
* Save the generated maven project

## Demo (Reactive endpoint) ##
In the just generated Spring WebFlux project lets build an REST endpoint getting generic store Item in the [Reactive way](https://en.wikipedia.org/wiki/Reactive_programming). First lets start with:

```
@RestController
public class ItemsReactiveController {

    @Autowired
    private IItemsService iItemsService;

    public Flux<Item> findById(String id) {
        try {
            System.out.println("Getting the data from DB...");
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Flux.just(iItemsService.findById(id));
    }

    @GetMapping(value = "/store/{id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public void getItemById(@PathVariable String id) {
        System.out.println("Controller start....");
        findById(id).subscribe(new Subscriber<Item>() {

            @Override
            public void onSubscribe(Subscription s) {
                s.request(Long.MAX_VALUE);
            }
            @Override
            public void onNext(Item t) {
                System.out.println("Retrieved: "+t.getName());
            }
            @Override
            public void onError(Throwable t) {
            }
            @Override
            public void onComplete() {
            }
        });
        System.out.println("End of method.");
    }
}
```

## Code in details ##

Method **findById** returns a Flux<Item> type. [Flux](https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Flux.html) is an datatype **returning 0..N** items in the reactive way. Other possibility is to use [Mono](https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Mono.html)<Item> type which returns 0 or 1 item. Jurgen Hoeller clearly mentioned that *if an endpoint returns one of the mentioned datatypes then there is actually no result returned, but Spring gives the caller a pipeline where result will land eventually*. Mechanism which is under the hood is very close to **EventLoop** as you know from NodeJS, but in the Spring way. To get any data from Reactive endpoint you need to subscribe on the returned pipeline. If you wanna get a callback on the result, subscription phase or error on the pipeline just use 
[Subscriber](http://www.reactive-streams.org/reactive-streams-1.0.0-javadoc/org/reactivestreams/Subscriber.html) from reactive streams which is the underlying implementation of [Project reactor](https://projectreactor.io). 

## First test ##
Lets invoke the previously created endpoint with {id} of the existing Item in the Oracle Store (I won't be bothering with used JPA configuration, it's not the subject of the demo). Hit in the browser: **http://localhost:8081/store/{id}**

Output at the side system output:
```
Controller start....
Getting the data from DB...
[EL Fine]: sql: 2017-03-20 14:19:56.321--ServerSession(26882836)--Connection(29914401)--SELECT ID, ITEM_NAME FROM ITEMS WHERE (ID = ?)
        bind => [1 parameter bound]
Retrieved: <Name of the Item>
End of method.
```
As you can see, the code did:

* Invoked the controller (Controller start....)
* Invoked the service with getting the item (Retrieved: <Name of the Item>)
* End of the method was reached. (End of method)

By default, getting the data from the subscription is happening on the main thread and of course **since I use datastore which doesn't provide reactive driver(Oracle) then calling into the store is blocking**. Currently, stores supporting Reactive programming(unblocking calls) are:

* Mongo
* Redis
* Cassandra
* Postgres

Definitely, check new project [Spring Data "Kay"](https://spring.io/blog/2016/11/28/going-reactive-with-spring-data) enabling reactive paradigm in the Spring Data project using the mentioned stores.

To actually enable publishing our Item asynchronously, we need to change the controller to:
```
package com.example.controller;

import com.example.domain.Item;
import com.example.service.IItemsService;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

/**
 * Created by Tomas.Kloucek on 17.3.2017.
 */
@RestController
public class ItemsReactiveController {

    @Autowired
    private IItemsService iItemsService;

    public Flux<Item> findById(String id) {
        try {
            System.out.println("Getting the data from DB...");
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Flux.just(iItemsService.findById(id)).publishOn(Schedulers.parallel());
    }

    @GetMapping(value = "/store/{id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public void getItemById(@PathVariable String id) {
        System.out.println("Controller start....");
        findById(id).subscribe(v -> {
           System.out.println("Consumed: "+v.getId());
        });
        System.out.println("End of method.");
    }
}
```
* I change the subscription for getting just the result. If you know RxJava you should be familiar. 
* I added publishOn method with scheduling the thread for publishing the Item asynchronously.

Now if we hit the endpoint again from the browser the output is going to be:

```
Controller start....
Getting the data from DB...
[EL Fine]: sql: 2017-03-20 14:16:49.69--ServerSession(18245293)--Connection(9048111)--SELECT ID, ITEM_NAME FROM ITEMS WHERE (ID = ?)
        bind => [1 parameter bound]
End of method.
Consumed: <ID of your Item entity>
```
As you can see **Spring reached now end of the method before he gave the subscription requested data**.

## Howto create a client to call Reactive Endpoint ##

Lets create another Spring Boot WebReactive application with code:
```
@SpringBootApplication
public class DemoApplication {

	@Bean
	public WebClient webClient() {
		return WebClient.create("http://<reactiveAppHost>:<reactiveAppPort>");
	}

	@Bean
	CommandLineRunner launch(WebClient webClient) {
		return args -> {
			webClient.get().Yuri("/store/{id}")
					.accept(MediaType.TEXT_EVENT_STREAM)
					.exchange()
					.flatMap(cr -> cr.bodyToFlux(Item.class))
					.subscribe(v -> {
						System.out.println("Received from MS: "+v.getName());
					});
		};
	}

	public static void main(String args[]) {
		new SpringApplicationBuilder(DemoApplication.class).properties
				(Collections.singletonMap("server.port", "8082"))
				.run(args);
	}
}
```

## Code in details ##

To call Reactive endpoint you need to obtain an [WebClient](http://docs.spring.io/spring/docs/5.0.0.BUILD-SNAPSHOT/javadoc-api/org/springframework/web/client/reactive/WebClient.html) instance first. In the demo case put into create method http://localhost:8081. **Own invocation is performed by WebClient.exchange() method call**, but to actually put a subscription on the pipeline for getting the result you need to call [ClientRequest](http://docs.spring.io/spring/docs/5.0.0.BUILD-SNAPSHOT/javadoc-api/org/springframework/web/client/reactive/ClientRequest.html).bodyToFlux(<ResultClassMapping>.class). Then subscription is possible. If we run the app then result should be:

```
Started DemoApplication in 4.631 seconds (JVM running for 4.954)
Received from MS: <Item text>
```

Full Reactive client code is at the following URL:

```
git clone https://tomask79@bitbucket.org/tomask79/spring-reactive-rest-client.git
```

## Is API for calling the REST endpoints asynchronously necessary? ##

My opinion on this new Reactive trend of calling the endpoints asynchronously with subscriptions and other stuff is kind of pessimistic. If I'd need to call endpoint asynchronously then JMS or AMPQ is a first idea which will hit me into the forehead especially in MicroServices. But we will see how this will turn out. Other planned changes in Spring Framework 5 are promising:

* [Functional Framework](https://spring.io/blog/2016/09/22/new-in-spring-5-functional-web-framework) with Angular like Router 
* Support for Project Jigsaw
* Lamdas in registering beans.

regards

Tomas